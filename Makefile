# Variables

MYINCLUDES=/home/ericp/libs/fftw-3.3.3
OPTIONS = -O3
CC = gcc
CFLAGS = #-Wall
LIBS = -L/home/ericp/libs/fftw-3.3.3 -lfftw3_threads -lfftw3 -lm -lpthread
INCLUDES =
SOURCES=complex.c
OBJECTS=$(SOURCES:.cpp=.o)
CTIS = ctis
DEFINES = -D REMOVE_NOISE=1 -D REPORT=1 -D 'FOCAL_PLANE="focal_plane"' -D PICTURE=1 -D 'PICTURE_OUT="reconstructed_image"' -D SEED=37 -D EPSILON=1e-8 -D MU=.01 -D LIM=2 -D RESTART=4 -D COMPUTE_PT=1 -D FTHREADS=4 -D THREADS=4 -D SYNTHETIC=1


all: circulant $(SOURCES) ctis

circulant: circulant.c
	$(CC) $(OPTIONS) $(CFLAGS) $< -o $@ $(LIBS)  -D SYNTHETIC=1

complex: complex.c
	$(CC) -c $<

ctis: complex
	$(CC) $(OPTIONS) $(CFLAGS) -o ctis ctis.c $(DEFINES) complex.o $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f ctis circulant *.o core *~