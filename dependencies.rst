=================
CTIS dependencies
=================


Installing FFTW 3.3.3
=====================

Get the FFTW 3.3.3 sources (usually are not compiled with thread support, at least for Ubuntu).


Download the sources
````````````````````

$ cd <desired location of sources>
$ wget http://www.fftw.org/fftw-3.3.3.tar.gz
$ tar -zxvf fftw-3.3.3.tar.gz 
$ cd fftw-3.3.3/


MPI compiler
````````````

You need an MPI compiler to be able to compile the FFTW3 package with thread support. To know if one is already available on your system, you can just look for one:

$ which mpicc

If nothing is returned, then you need to install one.

On Ubuntu / Debian machines:

$ sudo apt-get install libopenmpi-dev
$ which mpicc
/usr/bin/mpicc
$ 



Compilation and options
```````````````````````

Once the `mpicc` is installed and available, you can compile & install with the following options:


$ cd /path/to/fftw-3.3.3
$ ./configure --enable-threads --enable-mpi --prefix=$HOME/usr --enable-shared=yes
$ make
$ make install


